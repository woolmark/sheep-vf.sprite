import java.io.*;
import java.util.*;

import javax.microedition.lcdui.*;

import com.vodafone.v10.graphics.sprite.*;


/**
 *
 * スプライトを読み込み描く機能をACanvasに追加したクラスです。<br>
 * setSpriteによりスプライトを登録します。<br>
 * <br>
 * <xmp>
 * setPallet("/pallet");                     //このメソッドでパレット情報を読み込む
 * Sprite[] sprite = setSprite("/tmp.spr");  //このメソッドでスプライト情報を読み込む
 * 
 * drawSprite(sprite[0], 10, 10);            //スプライトを書き込む
 * </xmp>
 *
 * @author takkie
 * @version 0.9
 * @see VSCanvas
 *
 */
public abstract class VSCanvas extends SpriteCanvas{
	
	/**
	* 描くスプライトのデータスタック
	*/
	private Vector draw_sprite_stack;
	
	
	/**
	* スプライトを読み込んでいる
	*/
	public static final int SPRITE_LOAD = -1;
	
	
	/**
	* ステート
	*/
	protected int state;
	
	
	/**
	* ロードした数
	*/
	protected int loading_cnt;
	
	
	/**
	* ロードするスプライトの数
	*/
	protected int max_loading_cnt;
	
	
	/**
	* 画像とスプライトを保管する
	*/
	private Vector vec;
	
	/**
	*
	*/
	protected Sprite background;
	
	/**
	* VSCanvasコンストラクタ
	*
	* @param numPalettes - このクラスで使えるパレット数
	* @param numPatterns - このクラスで使えるパターン数
	* @param width - フレームバッファの幅
	* @param height - フレームバッファの高さ
	*
	*/
	public VSCanvas(int numPalettes, int numPatterns, int height, int width) {
		super(numPalettes, numPatterns);
    createFrameBuffer(height, width);
		draw_sprite_stack = new Vector();
		vec = new Vector();
	}
	
	
	/**
	* フレームバッファの内容を実画面に転送する。
	*
	* @param tx - 実画面のx座標
	* @param ty - 実画面のy座標
	*/
	public void flush(int tx, int ty) {
    copyArea(0, 0, 128, 111, 0, 0);
		writeAllSprite();
		drawFrameBuffer(tx, ty);
	}
	
	
	/**
	* 登録されているスプライトを初期化する
	*/
	public void clearSprite(){
		Sprite.totalIndex = 0;
	}
		
	/**
	* スプライトを書くメソッド
	*
	* @param sprite - どのスプライトを描くか
	* @param x - スプライトを描くx座標
	* @param y - スプライトを描くy座標
	*/
	public void drawSprite(Sprite sprite, int x, int y) {
		
		Sprite tmp = sprite.clone();
		
		tmp.x = x;
		tmp.y = y;
		tmp.turn = 0;
		
		draw_sprite_stack.addElement(tmp);
		
	}
	
	
	/**
	* スプライトを書くメソッド(上下左右反転ありバージョン)
	*
	* @param sprite - どのスプライトを描くか
	* @param x - スプライトを描くx座標
	* @param y - スプライトを描くy座標
	* @param turn - 反転 (0:回転なし 1:左右反転 2:上下反転 3:上下左右反転)
	*/
	public void drawSprite(Sprite sprite, int x, int y, int turn) {
		
		Sprite tmp = sprite.clone();
		
		tmp.x = x;
		tmp.y = y;
		tmp.turn = turn;
		
		draw_sprite_stack.addElement(tmp);
		
	}
	
	public void drawBackgroundSprite(Sprite sprite, int x, int y) {

    try {
		
			int index = sprite.index;
			
			int wid   = sprite.tileX;
			int hei   = sprite.tileY;
			
			
			//--------------------------------------------
			//スプライトを描画する
			for(int j=0;j<hei;j++) {
				for(int i=0;i<wid;i++) {
					
					short px = (short)x;
					short py = (short)y;
					
					px += (short)(i*8);
					py += (short)(j*8);
					
					drawBackground(createCharacterCommand(0, true, 0, false, false, index+j*wid+i), px, py);
				}
			}
			
		}catch(Exception e) {
		}
		
	}
	
	/**
	* sp_stackに格納されたスプライト全ての描画
	*/
	private void writeAllSprite() {
		//すべてのスプライト描画
		for(int i=0;i<draw_sprite_stack.size();i++) {
			Sprite sprite = (Sprite)draw_sprite_stack.elementAt(i);
			writeSprite(sprite);
		}
		if(background != null) {
			writeBackground(background);
		}
		
		//スタックの初期化
		draw_sprite_stack.removeAllElements();
		background = null;
	}
	
	
	/**
	* スプライトの描画 
	*/
	private void writeSprite(Sprite sprite) {
		try {
			
			//------------------------------------------
			//どのスプライトが調べる
			int index = sprite.index;

      System.out.println(sprite.index + ": " + sprite.x + "," + sprite.y);
			
			int x = sprite.x;
			int y = sprite.y;
			
			int wid   = sprite.tileX;
			int hei   = sprite.tileY;
			
			int turn  = sprite.turn;
			
			
			//-------------------------------------------
			//スプライトの描画
			boolean ud = false;
			boolean lr = false;
			
			
			//------------------------------------------
			//スプライトを回転する
			if((turn & 1) == 1) {
				lr = true;
			}
			if((turn & 2) == 2) {
				ud = true;
			}
			
			
			//--------------------------------------------
			//スプライトを描画する
			for(int j=0;j<hei;j++) {
				for(int i=0;i<wid;i++) {
					
					short px = (short)x;
					short py = (short)y;
					
					if(lr) {
						px += (short)((wid-1-i)*8);
					}
					else {
						px += (short)(i*8);
					}
					
					if(ud) {
						py += (short)((hei-1-j)*8);
					}
					else {
						py += (short)(j*8);
					}
					
					drawSpriteChar(createCharacterCommand(0, true, 0, ud, lr, index+j*wid+i), px, py);
				}
			}
			
		}catch(Exception e) {
		}
		
	}
	
	
	/**
	* スプライトの描画 
	*/
	private void writeBackground(Sprite sprite) {
		try {
			
			//------------------------------------------
			//どのスプライトが調べる
			int index = sprite.index;
			
			int x = sprite.x;
			int y = sprite.y;
			
			int wid   = sprite.tileX;
			int hei   = sprite.tileY;
			
			
			//--------------------------------------------
			//スプライトを描画する
			for(int j=0;j<hei;j++) {
				for(int i=0;i<wid;i++) {
					
					short px = (short)x;
					short py = (short)y;

					px += (short)(i*8);
					py += (short)(j*8);
					
					drawBackground(createCharacterCommand(0, true, 0, false, false, index+j*wid+i), (short)px, (short)py);
				}
			}
			
		}catch(Exception e) {
			System.out.println("back: " + e);
		}
		
	}

	
	/**
	* 読み込んだスプライトの数を返す
	*
	* @return 読み込んだスプライトの数を返す
	*/
	public int getLoadingCnt(){
		return loading_cnt;
	}
	
	
	/**
	* これから読むスプライトの枚数
	*
	* @return スプライトの枚数
	*/
	public int getMaxLoadingSprite(){
		return max_loading_cnt;
	}
	
	
	/**
	* スプライトを読み込む
	*
	* @param SPRITE_SIZE[] - スプライトのデータサイズ
	* @param NAME - 固めたデータの名前
	* @expception - スプライトが256を越えるとArrayIndexOutOfBoundsExceptionを返します
	*/
	public Sprite[] setSprite(String NAME) throws ArrayIndexOutOfBoundsException {
		return setSprite(NAME, 0);
	}
	
	
	/**
	* スプライトを読み込む
	*
	* @param NAME - スプライト名
	* @param num - 何番のスプライトか
	*/
	public Sprite[] setSprite(String NAME, int num) throws ArrayIndexOutOfBoundsException {
		
		InputStream in = getClass().getResourceAsStream(NAME);
		
		return setSprite(in, num);
		
	}
	
	
	/**
	* スプライトを読み込む
	*
	* @param SPRITE_SIZE[] - スプライトのデータサイズ
	* @param NAME - 固めたデータの名前
	* @param num - 何番目からのスプライトか
	* @expception - スプライトが256を越えるとArrayIndexOutOfBoundsExceptionを返します
	*/
	public Sprite[] setSprite(InputStream in, int num) throws ArrayIndexOutOfBoundsException {
		
		Sprite[] spr = null;
		
		try {
			
			//設定場所の指定
			Sprite.totalIndex = num;
			
			state = SPRITE_LOAD;
			
			
			//--------------------------------------------
			//スプライトの枚数を読み込む
			int size = in.read() & 0x000000ff;
			spr = new Sprite[size];
			
			
			//ローディングカウントを初期化する
			loading_cnt = 0;
			
			//何枚スプライトがあるか
			max_loading_cnt = size;
			
			
			for(int cnt=0;cnt<size;cnt++){
				
				
				//----------------------------------------------
				//スプライトのサイズ
				int len = (in.read() << 8) & 0x0000ff00;
				len |= in.read() & 0x000000ff;
				
				
				//-----------------------------------------------
				//スプライトのデータを読み込む
				byte[] buf = new byte[len+2];
				in.read(buf, 0, buf.length);
				
				
				//-----------------------------------------------
				//スプライトを造る
				spr[cnt] = addSprite(buf);
				
			}
			
			in.close();
		}catch(Exception e){
			throw new ArrayIndexOutOfBoundsException("スプライト数が256を越えました。");
		}
		
		return spr;
	}
	
	
	
	/**
	* スプライトを追加する
	*
	* @param buf - スプライトデータが格納されている
	* @return スプライトデータ
	*/
	public Sprite addSprite(byte[] buf){
		
		//----------------------------------------------
		//横幅、縦幅の読み込み 
		int wid = (int)(buf[0] & 0x0000ff);
		int hei = (int)(buf[1] & 0x0000ff);
		
		
		//----------------------------------------------
		//データの読み込み
		byte[] tmp_data = new byte[buf.length-2];
		for(int i=0;i<tmp_data.length;i++){
			tmp_data[i] = buf[i+2];
		}
		
		
		//----------------------------------------------
		//スプライトデータを作る
		byte[][][] sprite = byteArrayToSprite(tmp_data, wid, hei);
		
		
		//----------------------------------------------
		//スプライトデータのセット
		for(int i=0;i<sprite.length;i++) {
			for(int j=0;j<sprite[0].length;j++) {
				setPattern(Sprite.totalIndex+i*sprite[0].length+j, sprite[i][j]);
			}
		}
		
		//-----------------------------------------------
		//スプライトを設定する
		Sprite spr = new Sprite();
		
		spr.index = Sprite.totalIndex;
		spr.tileX = (wid+7)/8;
		spr.tileY = (hei+7)/8;
		spr.width = wid;
		spr.height = hei;
		
		
		//------------------------------------------------
		//トータルのスプライトの枚数の加算
		Sprite.totalIndex += ((wid+7)/8)*((hei+7)/8);
		
		
		return spr;
		
	}
	
	
	
	
	/**
	* パレットを読み込む
	*
	* @param PALATTE_SIZE[] - パレットのデータサイズ
	* @param NAME - 固めたデータの名前
	* @expception - パレットが256を越えるとArrayIndexOutOfBoundsExceptionを返します
	*/
	public void setPalette(String NAME) throws ArrayIndexOutOfBoundsException {
		try {
			
			InputStream in = getClass().getResourceAsStream(NAME);
			
			int size = in.read() & 0x000000ff;
			
			//------------------------------------------------
			//パレットの読み込み 
			for(int i=0;i<size;i++) {
				
				
				int len = in.read() & 0x000000ff;
				
				//-------------------------------------------------
				//パレットのデータを読み込む
				byte[] tmp_data = new byte[len];
				in.read(tmp_data, 0, tmp_data.length);
				
				
				//-------------------------------------------------
				//パレットの設定 
				for(int j=0;j<tmp_data.length;j+=3) {
					
					int t = 0;
					
					t |= ((tmp_data[j]<<16) & 0x00ff0000);
					t |= ((tmp_data[j+1]<<8) & 0x0000ff00);
					t |= (tmp_data[j+2] & 0x000000ff);
					
					setPalette(i*32+j/3, t);
				}
			}
			
			in.close();
			
		}catch(Exception e){
			throw new ArrayIndexOutOfBoundsException("パレット数が256を越えました。");
		}
		
	}
	
	
	
	
	
	
	/**
	* バイト配列からスプライトデータへの変換
	*
	* @param tmp_data - スプライトのバイナリデータ
	* @param wid - スプライトの横幅
	* @param hei - スプライトの縦幅
	*/
	private byte[][][] byteArrayToSprite(byte[] tmp_data, int wid, int hei) {
		
		
		//----------------------------------------------------
		//スプライトの枚数を計算
		int spwid = (wid+7)/8;
		int sphei = (hei+7)/8;
		
		
		
		//-----------------------------------------------------
		//スプライトデータに変換する
		byte[][][] sprite = new byte[sphei][spwid][64];
		try {
			int place = 0;
			for(int y=0;y<sphei;y++) {
				for(int x=0;x<spwid;x++) {
					for(int i=0;i<64;i++) {
						sprite[y][x][i] = tmp_data[place++];
					}
				}
			}
		}catch(Exception e){}
		
		
		return sprite;
	}
	
	
	/**
	* スプライトと画像の配列に追加する
	*
	* @param obj - 追加したい画像 or スプライト
	*/
	public void add(Object obj){
		vec.addElement(obj);
	}
	
	
	/**
	* スプライトと画像の入れるに、index番目に挿入する
	*
	* @param obj - 追加する画像 or スプライト
	* @param index - 追加するインデックス
	*/
	public void add(Object obj, int index){
		vec.insertElementAt(obj ,index);
	}
	
	
	/**
	* 削除したいスプライト or 画像
	*
	* @param index - 配列のインデックス
	*/
	public void remove(int index){
		vec.removeElementAt(index);
	}
	
	
	/**
	* 画像データ or スプライトデータのすべてを消す
	*/
	public void removeAll(){
		vec.removeAllElements();
	}
	
	
	/**
	* 画像データ or スプライトデータのサイズ
	*
	* @return int
	*/
	public int getLength(){
		return vec.size();
	}
	
	
	/**
	* スプライト or 画像を描く
	*
	* @param g - キャンバスのグラフィックコンテキスト
	* @param index - スプライトデータ or 画像データのインデックス
	* @param x - 描くx座標
	* @param y - 描くy座標
	*/
	public void draw(Graphics g, int index, int x, int y){
		try {
			draw(g, vec.elementAt(index), x, y);
		}catch(ArrayIndexOutOfBoundsException e){}
	}
	
	
	
	/**
	* スプライト or 画像を描く
	*
	* @param g - キャンバスのグラフィックコンテキスト
	* @param obj - スプライトデータ or 画像データ
	* @param x - 描くx座標
	* @param y - 描くy座標
	*/
	public void draw(Graphics g, Object obj, int x, int y){
		
		if(obj instanceof Sprite){
			drawSprite((Sprite)obj, x, y);
		}
		else if(obj instanceof Image){
			g.drawImage((Image)obj, x, y, g.TOP|g.LEFT);
		}
		
	}
	
}
