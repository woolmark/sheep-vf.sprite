@ECHO OFF

SETLOCAL

REM =======================================
REM
REM    make.bat  ver. 1.2
REM
REM =======================================
REM
REM [Purpose]
REM   J-Phone appli compile
REM
REM [Tag List]
REM   ALL        : make appli file
REM   PROGUARD   : make appli file with "proguard.jar"
REM   RETROGUARD : make appli file with "retroguard.jar"
REM   BASE       : compile only
REM   PRE        : preverify many classes
REM   PRO        : excute proguard
REM   RETRO      : excute retroguard 
REM   JAR        : jar compress
REM   DOC        : make the api document
REM   MF         : write a manifest file
REM   JAD        : write a jad file
REM
REM [Derectory]
REM   src        : source files
REM   res        : resource file
REM   classes    : class files
REM   bin        : binary file
REM
REM =======================================


REM =======================
REM    SET ENVIROMENT

SET JAR=sheepS.jar
SET JAD=sheepS.jad
SET MF=MANIFEST.MF

SET APPLI=sheep_S
SET CLASS=Sheep
SET ICON=icon.png
SET VENDOR=takkie
SET VERSION=1.0
SET OCL=VSCL-1.0.1
SET DSIZE=10240
SET DESCRIPTION=

SET JAVA_HOME=c:\j2sdk1.4.2_01
SET PROGUARD=C:\i-jade\proguard.jar
SET RETROGUARD=C:\i-jade\retroguard.jar
SET BOOT=C:\WTK20\lib\midpapi.zip
SET LIB=C:\eclipse\workspace\ete-vscl\lib\stubclasses.zip
SET PREVERIFY=C:\WTK20\bin\preverify.exe

REM =======================


REM =======================
REM    SELECT ACTION

IF "%1" == "" GOTO ALL
GOTO %1

REM =======================


REM =======================
REM    MAKE BINARY FILE

:ALL

CALL :MF
CALL :BASE
CALL :PRE
CALL :JAR
CALL :JAD

GOTO :EOF

REM =======================


REM =======================
REM    MAKE BINARY FILE WITH PROGUARD

:PROGUARD

CALL :MF
CALL :BASE
CALL :PRO
CALL :PRE
CALL :JAR
CALL :JAD

GOTO :EOF

REM =======================


REM =======================
REM    MAKE BINARY FILE WITH RETROGUARD

:RETROGUARD

CALL :MF
CALL :BASE
CALL :RETRO
CALL :PRE
CALL :JAR
CALL :JAD

GOTO :EOF

REM =======================


REM =======================
REM    COMPILE

:BASE

DEL /Q classes\*
%JAVA_HOME%\bin\javac -g:none -d classes -bootclasspath %BOOT% -classpath %LIB% src\*.java

GOTO :EOF

REM =======================


REM =======================
REM    PREVERIFY 

:PRE

CD classes
FOR %%A IN (*.class) DO %PREVERIFY% -d . -classpath %BOOT%;%LIB%;. %%~nA
CD ..

GOTO :EOF

REM =======================


REM =======================
REM    PROGUARD

:PRO

%JAVA_HOME%\bin\jar cvfM in.jar -C classes .
DEL /Q classes\*
%JAVA_HOME%\bin\java -jar %PROGUARD% -injar in.jar -outjar out.jar -libraryjars %LIB% -keep class %CLASS% { public void startApp(); public void pauseApp(); public void destroyApp(); }
DEL in.jar
MOVE out.jar classes
CD classes
%JAVA_HOME%\bin\jar xf out.jar
DEL out.jar
CD ..

GOTO :EOF

REM =======================


REM =======================
REM    RETROGUARD

:RETRO

%JAVA_HOME%\bin\jar cvfM in.jar -C classes .
DEL /Q classes\*
%JAVA_HOME%\bin\java -classpath %BOOT%;%LIB%;%RETROGUARD% RetroGuard
DEL in.jar
MOVE out.jar classes
CD classes
%JAVA_HOME%\bin\jar xf out.jar
DEL out.jar
RMDIR /S/Q META-INF
CD ..

GOTO :EOF

REM =======================


REM =======================
REM    JAR COMPRESS

:JAR

%JAVA_HOME%\bin\jar cvfm bin\%JAR% bin\MANIFEST.MF -C classes .
%JAVA_HOME%\bin\jar uvfm bin\%JAR% bin\MANIFEST.MF -C res .

GOTO :EOF

REM =======================


REM =======================
REM    MAKE JAVADOC

:DOC

%JAVA_HOME%\bin\javadoc -d doc -classpath %BOOT%;%LIB% -private -author -version -windowtitle "HelloWorld" src\*.java

GOTO :EOF

REM =======================


REM =======================
REM    MAKE MANIFEST FILE

:MF

ECHO MIDlet-1: %APPLI%, /%ICON%, %CLASS%> bin\%MF%
ECHO MIDlet-Data-Size: %DSIZE%>> bin\%MF%
ECHO MIDlet-Description: %DESCRIPTION%>> bin\%MF%
ECHO MIDlet-Name: %APPLI%>> bin\%MF%
ECHO MIDlet-Version: %VERSION%>> bin\%MF%
ECHO MIDlet-Vendor: %VENDOR%>> bin\%MF%
ECHO MicroEdition-Configuration: CLDC-1.0>> bin\%MF%
ECHO MicroEdition-Profile: MIDP-1.0>> bin\%MF%

GOTO :EOF

REM =======================


REM =======================
REM    MAKE JAD FILE

:JAD

FOR %%A IN (bin\%JAR%) DO SET SIZE=%%~zA

ECHO MIDlet-1: %APPLI%, /%ICON%, %CLASS%> bin\%JAD%
REM ECHO MIDlet-Icon: /%ICON%>> bin\%JAD%
ECHO MIDlet-Description: %DESCRIPTION%>> bin\%JAD%
ECHO MIDlet-Name: %APPLI%>> bin\%JAD%
ECHO MIDlet-Vendor: %VENDOR%>> bin\%JAD%
ECHO MIDlet-Version: %VERSION%>> bin\%JAD%
ECHO MicroEdition-Configuration: CLDC-1.0>> bin\%JAD%
ECHO MicroEdition-Profile: MIDP-1.0>> bin\%JAD%
ECHO MIDlet-Install-Notify: sheep>> bin\%JAD%
ECHO MIDxlet-API: %OCL%>> bin\%JAD%
ECHO MIDlet-Jar-Size: %SIZE%>> bin\%JAD%
ECHO MIDlet-Data-Size: %DSIZE%>> bin\%JAD%
ECHO MIDlet-Jar-URL: http://ev/v/a/%JAR%>> bin\%JAD%
REM ECHO MIDlet-Jar-URL: %JAR%>> bin\%JAD%

GOTO :EOF

REM =======================

REM =======================
REM    MAKE DIR

:md

MKDIR src
MKDIR classes
MKDIR bin
MKDIR res

GOTO :EOF

REM =======================


REM =======================
REM    COPY

:copy

COPY bin\sheepS.ja? C:\cygwin\home\httpd\ev\v\a

GOTO :EOF

REM =======================


